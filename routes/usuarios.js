const express = require('express');
const Usuario = require('../models/usuario_model');
const Joi = require('joi');
const router = express.Router();

const schema = Joi.object({
    nombre: Joi.string()
        .min(3)
        .max(15)
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
})



router.get('/', (req, res)=>{
    let resultado = listarUsuariosActivos();
    resultado.then(usuarios =>{
        res.json(usuarios)
    }).catch(error => {
        res.status(400).json({
            error: error
        })
    })
});

router.post('/', (req, res) => {
    let body = req.body;
    const {error, value } = schema.validate({nombre: body.nombre, email: body.email})
    if (!error){
        let peticion = crearUsuario(body);

        peticion.then( response => {
            res.json({
                valor: response
            })
        }). catch( error => {
            res.status(400).json({
                error: error
            })
        })
    }else {
        res.status(400).json({
            error
        })
    }

    
});

router.put('/:email', (req, res)=>{
    let body = req.body;
    const {error, value } = schema.validate({nombre: body.nombre})
    if (!error){
        let resultado = actualizarUsuario(req.params.email, req.body);
        resultado.then(valor => {
            res.json({
                valor: valor
            })
        }).catch(error => {
            res.status(400).json({
                error: error
            })
        });
    }else {
        res.status(400).json({
            error
        })
    }
    
});

router.delete('/:email', (req, res) => {
    let resultado = desactivarUsuario(req.params.email);
    resultado.then(response => {
        res.json({
            usuario: response
        })
    }).catch(error => {
        res.status(400).json({
            error: error
        })
    })
});



/**FUNCIONES PARA REALIZAR LAS OPERACIONES CON LA BASE DE DATOS */
async function crearUsuario(body){
    let usuario = new Usuario({
        email    : body.email,
        nombre    : body.nombre,
        password    : body.password,
    })

    return await usuario.save();
}

async function listarUsuariosActivos(){
    let usuarios = await Usuario.find({"estado": true})

    return usuarios;
}

async function actualizarUsuario(email, body){
    let usuario = await Usuario.findOneAndUpdate({"email": email}, {
        $set: {
            nombre: body.nombre,
            password: body.password
        }
    }, {new: true});

    return usuario;
}

async function desactivarUsuario(email){
    let usuario = await Usuario.findOneAndUpdate({"email":email}, {
        $set: {
            estado: false
        }
    }, {new: true});

    return usuario;
}

module.exports = router;