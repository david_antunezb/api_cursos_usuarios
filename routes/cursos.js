const { response } = require('express');
const express = require('express');
const Curso = require('../models/curso_model');
const router = express.Router();

router.get('/', (req, res)=>{
    let peticion = listarCursosActivos();
    peticion.then(cursos =>{
        res.json(cursos)
    }).catch(error => {
        res.status(400).json({
            error: error
        })
    })
});

router.post('/', (req, res)=>{
    let body = req.body;
    let peticion = crearCurso(body);

    peticion.then(response=>{
        res.json({
            response
        })
    }).catch(error => {
        res.status(400).json({
            error
        })
    })
});

router.put('/:id', (req, res)=>{
    let peticion = actualizarCurso(req.params.id, req.body);
    peticion.then(response=>{
        res.json({
            response
        })
    }).catch(error => {
        res.status(400).json({
            error
        })
    })
});

router.delete('/:id', (req, res)=>{
    let peticion = desactivarCurso(req.params.id);
    peticion.then(response =>{
        res.json({
            response
        })
    }).catch(error => {
        console.log(error);
        res.status(400).json({
            error
        })
    })
});




async function crearCurso(body){
    let curso = new Curso({
        titulo: body.titulo,
        descripcion: body.desc
    })

    return await curso.save();
}

async function listarCursosActivos(){
    let cursos = await Curso.find({"estado": true})

    return cursos;
}

async function actualizarCurso(id, body){
    let curso = await Curso.findByIdAndUpdate(id, {
        $set: {
            titulo: body.titulo,
            descripcion: body.desc
        }
    }, {new: true});

    return curso;
}

async function desactivarCurso(id){
    let curso = await Curso.findByIdAndUpdate(id, {
        $set: {
            estado: false
        }
    }, {new: true});

    return curso;
}

module.exports = router;