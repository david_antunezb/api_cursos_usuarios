const usuarios = require('./routes/usuarios');
const cursos = require('./routes/cursos');

const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/api/usuarios', usuarios);
app.use('/api/cursos', cursos);


mongoose.connect('mongodb://localhost/cursos_api')
    .then(()=> console.log('Conectado a MongoDB...'))
    .catch(error => console.log('No se pudo conectar con MongoDB.. ERROR: ' + error))




app.listen(PORT, ()=>{
    console.log('Api RestFull Ejecutandose...');
})
